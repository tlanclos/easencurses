#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tlanclos'

import os, sys
try:
    import curses
    import curses.textpad
    import locale
    from copy import deepcopy as dcpy
    from atexit import register
except ImportError, err:
    print err
    sys.exit(1)

class Easencurses():
    COLOR_PAIRS = {
        name: (i + 1, fg, bg) for (i, (name, fg, bg)) in
        enumerate(
            [
                ('FAIL', curses.COLOR_BLACK, curses.COLOR_RED),
                ('OKAY', curses.COLOR_BLACK, curses.COLOR_GREEN)
            ]
        )
    }

    CHARACTER_ACTIONS = {
        'NORMAL': curses.A_NORMAL,
        'BLINK': curses.A_BLINK
    }

    @staticmethod
    def _loadPairs():
        for key in Easencurses.COLOR_PAIRS.keys():
            (i, fg, bg) = Easencurses.COLOR_PAIRS[key]
            curses.init_pair(i, fg, bg)

    @staticmethod
    def status(message):
        print '<EASENCURSES.PY> %s' % message

    class Window:
        def __init__(self, x, y, width, height, border=False, borderSetup=None):
            self.__hasBorder = border
            self.__width = width
            self.__height = height
            if self.__hasBorder is not None:
                self.__borderWin = curses.newwin(self.__height + 2, self.__width + 2, y, x)
                self.__win = curses.newwin(self.__height, self.__width, y + 1, x + 1)
                if borderSetup is not None:
                    self.setBorder(borderSetup)
                else:
                    self.setBorder([0, 0, 0, 0, 0, 0, 0, 0])
            else:
                self.__win = curses.newwin(self.__height, self.__width, y, x)
            self.__borderWin.refresh()
            self.__win.refresh()
            self.__win.immedok(True)
            self.__borderWin.immedok(True)

        def addString(self, x, y, string, color=None, action='NORMAL'):
            if color is not None:
                (i, fg, bg) = Easencurses.COLOR_PAIRS[color]
                color = curses.color_pair(i)
            else:
                color = 0
            action = Easencurses.CHARACTER_ACTIONS[action]
            self.__win.addnstr(y, x, string, self.__width - x, color | action)
            self.__win.refresh()

        def getWindow(self):
            return self.__win

        def clear(self):
            self.__win.clear()
            self.__refresh__()

        def setBorder(self, array):
            self.__borderWin.border(
                array[0], array[1], array[2],
                array[3], array[4], array[5],
                array[6], array[7]
            )
            self.__borderWin.refresh()

        def __refresh__(self):
            self.__win.refresh()

        def __uninit__(self):
            self.__win.clear()
            self.__win.refresh()
            del self.__win

    class VTable:
        def __init__(self, x, y, height, columnWidthsArray, border=True):
            self.__numcols = len(columnWidthsArray)
            self.__columns = [None for n in range(self.__numcols)]
            self.__hasBorder = border
            lastX = x
            for (i, width) in enumerate(columnWidthsArray):
                if i == 0:
                    setup = [0, 0, 0, 0, 0, 0, 0, 0]
                else:
                    setup = [0, 0, 0, 0, curses.ACS_TTEE, 0, curses.ACS_BTEE, 0]
                self.__columns[i] = Easencurses.Window(lastX, y, width, height, border=self.__hasBorder, borderSetup=setup)
                lastX += width + 1

        def addString(self, column, row, string, color=None, action='NORMAL'):
            self.__columns[column].addString(0, row, string, color=color, action=action)

        def clearColumn(self, column):
            self.__columns[column].clear()

        def clear(self):
            [self.__columns[i].clear() for i in range(self.__numcols)]

        def _getColumn(self, column):
            return self.__columns[column]

        def __refresh__(self):
            [c.__refresh__() for c in self.__columns]

        def __uninit__(self):
            for i in range(self.__numcols):
                self.__columns[i].__uninit__()

    class HTable:
        def __init__(self, x, y, width, rowHeightArray, border=True):
            self.__numrows = len(rowHeightArray)
            self.__rows = [None for n in range(self.__numrows)]
            self.__hasBorder = border
            lastY = y
            for (i, height) in enumerate(rowHeightArray):
                if i == 0:
                    setup = [0, 0, 0, 0, 0, 0, 0, 0]
                else:
                    setup = [0, 0, 0, 0, curses.ACS_LTEE, curses.ACS_RTEE, 0, 0]
                self.__rows[i] = Easencurses.Window(x, lastY, width, height, border=self.__hasBorder, borderSetup=setup)
                lastY += height + 1

        def addString(self, column, row, string, color=None, action='NORMAL'):
            self.__rows[row].addString(column, 0, string, color=color, action=action)

        def clearRow(self, row):
            self.__rows[row].clear()

        def clear(self):
            [self.__rows[i].clear() for i in range(self.__rows)]

        def _getRow(self, row):
            return self.__rows[row]

        def __refresh__(self):
            [r.__refresh__() for r in self.__rows]

        def __uninit__(self):
            for i in range(self.__numrows):
                self.__rows[i].__uninit__()

    class InputPrompt:
        def __init__(self, x, y, inputWidth, height, message, border=False):
            self.__hasBorder = border
            self.__msgLength = len(message) + 1
            self.__msgWin = Easencurses.Window(x, y, self.__msgLength, height, border=self.__hasBorder, borderSetup=None)
            self.__msgWin.addString(0, 0, message)
            self.__edtWin = Easencurses.Window(self.__msgLength + x, y, inputWidth, height, border=self.__hasBorder,
                                               borderSetup=[0, 0, 0, 0, curses.ACS_TTEE, 0, curses.ACS_BTEE, 0])

        def getInput(self):
            edtBox = curses.textpad.Textbox(self.__edtWin.getWindow(), True)
            edtBox.edit()
            data = edtBox.gather()
            del edtBox
            return str(data.strip())

        def clear(self):
            self.__msgWin.clear()
            self.__edtWin.clear()

        def _getMsgWin(self):
            return self.__msgWin

        def _getEditWin(self):
            return self.__edtWin

        def __refresh__(self):
            self.__msgWin.__refresh__()
            self.__edtWin.__refresh__()

        def __uninit__(self):
            self.__msgWin.__uninit__()
            self.__edtWin.__uninit__()

    def __init__(self):
        register(self.__uninit__)
        locale.setlocale(locale.LC_ALL, '')
        self.__screen = curses.initscr()
        curses.start_color()
        curses.noecho()
        curses.cbreak()
        self.__screen.keypad(0)
        self.__objects = dict()
        Easencurses._loadPairs()

    def addObject(self, object, objectName):
        if objectName not in self.__objects.keys():
            self.__objects[objectName] = object
        else:
            Easencurses.status('object name already exists...')

    def removeObject(self, objectName):
        self.__objects[objectName].__uninit__()
        del self.__objects[objectName]

    def getObject(self, windowName):
        return self.__objects[windowName]

    def clearall(self):
        [self.__objects[key].clear() for key in self.__objects.keys()]

    def __uninit__(self):
        [self.__objects[key].__uninit__() for key in self.__objects.keys()]
        curses.nocbreak()
        self.__screen.keypad(0)
        curses.echo()
        curses.endwin()

if __name__ == '__main__':
    e = Easencurses()
    e.addObject(Easencurses.VTable(0, 0, 20, [20, 40, 5, 8], border=True), 'table1')
    e.addObject(Easencurses.Window(40, 20, 20, 12, border=True), 'win')

    e.getObject('win').addString(0, 0, 'hello', color='FAIL', action='BLINK')
    e.getObject('win').addString(19, 5, 'hello2', color='OKAY')

    e.getObject('table1').addString(2, 10, 'yddddddes it wddddddddordddkddddddddddddsed')

    e.addObject(Easencurses.HTable(3, 22, 20, [3, 5, 5], border=True), 'table2')
    e.getObject('table2').addString(2, 1, 'bsdlfsklajdfasdlfkjsdfa', color='OKAY', action='BLINK')
    e.addObject(Easencurses.InputPrompt(0, 38, 32, 1, 'messsageeeeeee ', border=True), 'input')
    input = e.getObject('input').getInput()